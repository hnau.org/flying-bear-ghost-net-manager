#!/usr/bin/env python3

from command import Command
from config import Config


class CommandAddress(Command):
    _config = Config()

    def get_name(self):
        return 'address'

    def get_description(self):
        return 'Get or set net address of printer'

    def execute(self, args):
        if len(args) < 1:
            address = self._config.get_address()
            address = 'unknown' if address is None else address
            print(f'Address: {address}')
            return
        address = args[0]
        self._config.set_address(address)
        print(f'Address: "{address}" was saved')
