#!/usr/bin/env python3

from command import Command
from config import Config
from http_client import HttpClient


class PrinterStatus:

    def __init__(self, response_content):
        if response_content.find(b"DATA") != 0:
            raise Exception(f"Response is {response_content}. Must be started by 'DATA'")

        response_content = response_content[4:]
        status = response_content.split(b",")

        if len(status) != 18:
            raise Exception(f"Response is {response_content}. Expected 18 elements")

        self._connect_state = int(status[0])
        self._is_bed_heating = status[1] == "1"
        self._is_nozzle_heating0 = status[2] == "1"
        self._is_nozzle_heating1 = status[3] == "1"
        self._is_printing = status[4] == "1"
        self._is_paused = status[5] == "1"
        self._temp_bed_target = int(status[6])
        self._temp_bed = int(status[7])
        self._temp_nozzle_target0 = int(status[8])
        self._temp_nozzle0 = int(status[9])
        self._temp_nozzle_target1 = int(status[10])
        self._temp_nozzle1 = int(status[11])
        self._nozzle_count = int(status[12])
        self._current_tool = int(status[13])
        self._printing_progress = int(status[14])
        self._print_version_code = int(status[15])
        self._print_id = int(status[16])
        if self._is_printing:
            self._printing_filename = status[17]
        else:
            self._printing_filename = ""

    def __str__(self):
        result = "\nPrinter status: \n"
        result += "Connection state: " + str(self._connect_state) + "\n"
        result += "Is bed heating: " + str(self._is_bed_heating) + "\n"
        result += "Is nozzle 1 heating: " + str(self._is_nozzle_heating0) + "\n"
        if self._nozzle_count >= 2:
            result += "Is nozzle 2 heating: " + str(self._is_nozzle_heating1) + "\n"
        result += "Is printing: " + str(self._is_printing) + "\n"
        result += "Is paused: " + str(self._is_paused) + "\n"
        result += "Bed temp target: " + str(self._temp_bed_target) + "\n"
        result += "Bed temp: " + str(self._temp_bed) + "\n"
        result += "Nozzle 1 temp target: " + str(self._temp_nozzle_target0) + "\n"
        result += "Nozzle 1 temp: " + str(self._temp_nozzle0) + "\n"
        if self._nozzle_count >= 2:
            result += "Nozzle 2 temp target: " + str(self._temp_nozzle_target1) + "\n"
            result += "Nozzle 2 temp: " + str(self._temp_nozzle1) + "\n"
        result += "Nozzles count: " + str(self._nozzle_count) + "\n"
        result += "Current tool: " + str(self._current_tool) + "\n"
        result += "Printing progress: " + str(self._printing_progress) + "\n"
        result += "Printing version code: " + str(self._print_version_code) + "\n"
        result += "Print id: " + str(self._print_id) + "\n"
        result += "Printing filename: " + str(self._printing_filename)
        return result


class CommandStatus(Command):
    _config = Config()

    def get_name(self):
        return 'status'

    def get_description(self):
        return 'Get status of printer'

    def execute(self, args):
        response = HttpClient().send("/status")
        status = PrinterStatus(response)
        print(status)
