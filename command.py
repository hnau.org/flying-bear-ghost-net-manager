#!/usr/bin/env python3


class Command:

    def execute(self, args):
        raise NotImplementedError

    def get_name(self):
        raise NotImplementedError

    def get_description(self):
        raise NotImplementedError
