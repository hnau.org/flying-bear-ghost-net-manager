#!/usr/bin/env python3

import http.client
from config import Config
import json
from dataclasses import dataclass


@dataclass
class FileToUpload:
    srcPath: str
    dstPath: str


class HttpClientMeta(type):
    _instance = None

    def __call__(cls):
        if cls._instance is None:
            cls._instance = super().__call__()
        return cls._instance


class HttpClient(metaclass=HttpClientMeta):
    _config = Config()
    _connect_path = "/connect"
    _session_error_code = 99

    def send(self, path, file_to_upload=None):
        address = self._config.get_address()
        if not address:
            raise Exception('Printer address is not defined')
        session_id = self._config.get_session_id()
        if path != self._connect_path and session_id < 0:
            print("Printer is not connected. Connecting...")
            return self._connect(lambda: self.send(path))
        connection = http.client.HTTPConnection(address)
        if file_to_upload:
            file = open(file_to_upload.srcPath, "rb")
            file_bytes = bytearray(file.read())
            connection.request(
                "POST",
                path,
                body=file_bytes,
                headers={
                    "session_id": session_id,
                    "path": file_to_upload.dstPath,
                    "Content-Type": "application/octet-stream"
                }
            )
        else:
            connection.request(
                "GET",
                path,
                headers={
                    "session_id": session_id
                }
            )
        response = connection.getresponse()
        code = response.status
        content = response.read()
        if code < 200 or code >= 300:
            raise Exception(
                f"Error while sending http request. Code: {code}, reason: {response.reason}, content: {content}"
            )
        if content:
            if content.find(b"{") >= 0:
                body = json.loads(content)
                code = body["c"]
                if code == self._session_error_code:
                    print("Need to reconnect to printer. Reconnecting...")
                    return self._connect(lambda: self.send(path))
        return content

    def _connect(self, after):
        response = self.send(self._connect_path)
        if not response:
            raise Exception("Unable to connect to printer. No response")
        body = json.loads(response)
        code = body["c"]
        message = body["m"]
        if code != 0:
            raise Exception(f"Unable to connect to printer. Code: {code}. Message: {message}")
        session_id = int(message)
        print(f"Connected to printer. Session id is {session_id}")
        self._config.set_session_id(session_id)
        return after()
