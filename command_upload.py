#!/usr/bin/env python3

from command import Command
from http_client import HttpClient
from http_client import FileToUpload
import os


class CommandUpload(Command):
    _max_file_name_length = 27

    def get_name(self):
        return 'upload'

    def get_description(self):
        return 'Upload file to printer'

    def execute(self, args):

        def create_dst_file_name(file_path):
            name = os.path.basename(file_path)
            file_name_without_ext, file_ext = os.path.splitext(name)
            file_name_length_limit = self._max_file_name_length - len(file_ext)
            if len(file_name_without_ext) > file_name_length_limit:
                file_name_without_ext = file_name_without_ext[0:file_name_length_limit]
            name = file_name_without_ext + file_ext
            return f"SD://{name}"

        if len(args) < 1:
            raise Exception("File to upload was not presented")
        file_to_uplod_path = args[0]
        file_name = create_dst_file_name(file_to_uplod_path)
        file_to_upload = FileToUpload(file_to_uplod_path, file_name)
        HttpClient().send("/upload", file_to_upload)
