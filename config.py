#!/usr/bin/env python3

import configparser


class ConfigMeta(type):
    _instance = None

    def __call__(cls):
        if cls._instance is None:
            cls._instance = super().__call__()
        return cls._instance


class Config(metaclass=ConfigMeta):
    _path = 'config.ini'
    _main_section_name = 'MAIN'
    _address_option_name = 'address'
    _session_id_option_name = 'session_id'

    def _get_from_main(self, option, fallback):
        return self._config.get(self._main_section_name, option, fallback=fallback)

    def _set_to_main(self, option, value):
        self._main[option] = value
        self._store()

    def __init__(self):
        self._config = configparser.ConfigParser()
        self._config.read(self._path)
        if not self._config.has_section(self._main_section_name):
            self._config.add_section(self._main_section_name)
        self._main = self._config[self._main_section_name]

    def _store(self):
        self._config.write(open(self._path, 'w'))

    def get_address(self):
        return self._get_from_main(self._address_option_name, "")

    def set_address(self, address):
        self._set_to_main(self._address_option_name, address)

    def get_session_id(self):
        return int(self._get_from_main(self._session_id_option_name, "-1"))

    def set_session_id(self, session_id):
        self._set_to_main(self._session_id_option_name, str(session_id))
