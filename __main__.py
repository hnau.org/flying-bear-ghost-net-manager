#!/usr/bin/env python3

import sys
from command_address import CommandAddress
from command_status import CommandStatus
from command_upload import CommandUpload


def main():
    commands = [
        CommandAddress(),
        CommandStatus(),
        CommandUpload()
    ]

    args = sys.argv[1:]

    if len(args) < 1:
        print('Available commands:')
        for command in commands:
            print(f' * {command.get_name()}. {command.get_description()}')
        return

    command_name = args[0]

    for command in commands:
        if command_name == command.get_name():
            command.execute(args[1:])
            return

    print(f'Command with name "{command_name}" not found')


main()
